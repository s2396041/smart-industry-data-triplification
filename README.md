# RDF Data Transformation Script

This script transforms CSV data into an RDF graph using the SAREF ontology. The resulting graph is serialized into a Turtle (.ttl) file.

## Features

- Loads data from a specified CSV file.
- Initializes an RDF graph and namespaces.
- Defines building types and hierarchy.
- Processes data from each meter in the CSV file.
- Adds measurements and related information to the RDF graph.
- Exports the RDF graph to a Turtle file.

## Prerequisites

- Python 3.x
- Required Python packages: `pandas`, `rdflib`

## Installation

1. Clone the repository:
   ```bash
   git clone https://github.com/yourusername/your-repo.git
   cd your-repo
   ```

2. Install the required packages:
   ```bash
   pip install pandas rdflib
   ```

## Usage

1. Prepare your CSV file with the expected format. The CSV should have the following columns:
   - `utc_timestamp`: Timestamp of the measurement.
   - `DE_KN_<BuildingType>_<MeterName>`: Measurement values for different meters in various buildings. Replace `<BuildingType>` with `industrial`, `public`, or `residential`.

2. Run the script with the path to your CSV file:
   ```bash
   python transform.py /path/to/your/data.csv
   ```

   Example:
   ```bash
   python transform.py data/building_energy.csv
   ```

## Script Details

### `load_data(file_path: str) -> None`
Loads the CSV data, initializes the RDF graph, and processes each meter's data to populate the graph.

- **Parameters**:
  - `file_path` (str): The path to the CSV data file.

### `main() -> None`
Sets up argument parsing for the script and calls `load_data()` with the provided file path.

## Output

The script generates a Turtle file named `graph.ttl` containing the RDF data.

## Example

Given a CSV file `data/building_energy.csv` with the following content:

```csv
utc_timestamp,DE_KN_industrial_Meter1,DE_KN_public_Meter2,DE_KN_residential_Meter3
2023-01-01T00:00:00Z,100,200,300
2023-01-01T01:00:00Z,150,250,350
```

Running the script:

```bash
python transform.py data/building_energy.csv
```

Will produce an RDF graph in `graph.ttl` representing the measurements and metadata for the specified meters and buildings.
