import pandas as pd
import argparse
from rdflib import Graph, Literal, Namespace, URIRef
from rdflib.namespace import RDF, RDFS, XSD


def load_data(file_path: str) -> None:
    print(f'[info] Loading data from {file_path}...')

    # Load CSV columns
    data = pd.read_csv(file_path)

    # Initialize RDF graph and namespace
    graph = Graph()
    saref = Namespace('https://saref.etsi.org/core/')
    base_url = 'http://example.org/'

    # Define building types
    industrial_building = URIRef(f'{base_url}IndustrialBuilding')
    public_building = URIRef(f'{base_url}PublicBuilding')
    residential_building = URIRef(f'{base_url}ResidentialBuilding')

    # Define hierarchy
    graph.add((industrial_building, RDFS.subClassOf, saref.Building))
    graph.add((public_building, RDFS.subClassOf, saref.Building))
    graph.add((residential_building, RDFS.subClassOf, saref.Building))

    # Define measurement unit
    watt_hour_unit = URIRef('https://w3id.org/saref#watt_hour')

    # Iterate over columns
    for col in data.columns.tolist():
        # Check if the column has the expected prefix
        if not col.startswith('DE_KN_'):
            continue

        # Extract building and meter information
        building_name = col[6:].split('_')[0]
        meter_name = col[6:].replace('_', '-')
        building_type = ''.join([x for x in building_name if x.isalpha()])

        print(f'[info] Processing data from meter {meter_name}...')

        # Create building URI
        building = URIRef(f'{base_url}{building_name}')

        # Define building type
        if building_type == 'industrial':
            graph.add((building, RDF.type, industrial_building))
        elif building_type == 'public':
            graph.add((building, RDF.type, public_building))
        elif building_type == 'residential':
            graph.add((building, RDF.type, residential_building))

        # Create meter IRI
        meter = URIRef(f'{base_url}{meter_name}')

        # Add data and references to meter
        graph.add((meter, RDF.type, saref.Meter))
        graph.add((meter, saref.measuresProperty, saref.Energy))
        graph.add((meter, saref.hasFunction, saref.MeteringFunction))
        graph.add((meter, RDFS.label, Literal(meter_name, lang="en")))
        graph.add((meter, URIRef(f'{base_url}location'), building))

        # Add measurements to meter
        measurement_counter = 1
        for index, row in data.iterrows():
            timestamp = row['utc_timestamp']
            value = row[col]

            # Check if the field has a value
            if pd.isnull(value):
                continue

            # Create measurement IRI
            measurement = URIRef(f'{base_url}{meter_name}-measurement-{measurement_counter}')

            # Add measurement data and references
            graph.add((measurement, RDF.type, saref.Measurement))
            graph.add((measurement, saref.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
            graph.add((measurement, saref.isMeasuredIn, watt_hour_unit))
            graph.add((measurement, saref.hasValue, Literal(value, datatype=XSD.decimal)))
            graph.add((measurement, saref.relatesToProperty, saref.Energy))
            graph.add((meter, saref.makesMeasurement, measurement))

            # Increment measurement counter
            measurement_counter += 1

    print(f'[info] Exporting turtle graph...')

    # Serialize graph to ttl file
    graph.serialize(destination='graph.ttl', format='turtle')

    print("[info] Graph saved to graph.ttl")


def main() -> None:
    # Setup argument parser
    parser = argparse.ArgumentParser(description='Transform CSV data to RDF graph using SAREF ontology.')
    parser.add_argument('file_path', type=str, help='Path to the CSV data file')

    # Parse command-line arguments
    args = parser.parse_args()
    file_path = args.file_path

    # Check if file path is provided
    if not file_path:
        print('[error] Please provide a file path to the data file')
        exit(1)

    # Load and process data
    load_data(file_path)


if __name__ == "__main__":
    main()
